import playground

from random import randint as rand

class Atom:
    def __init__(self, x, y, s_x, s_y, r):
        self.x = x
        self.y = y
        self.s_x = s_x
        self. s_y = s_y
        self.r = r

    def to_tuple(self):
        return (self.x, self.y, self.r)

    def move(self, width, height):

            if (self.x + self.s_x + self.r) >= width or (self.x + self.s_x + self.r) <= 0:
                self.s_x *= -1
            
            self.x += self.s_x

            if (self.y + self.s_y + self.r) >= height or (self.y + self.s_y + self.r) <= 0:
                self.s_y *= -1
            
            self.y += self.s_y


class ExampleWorld(object):

    def __init__(self, size_x, size_y, count_of_atoms):
        self.width = size_x
        self.height = size_y
        self.atoms = []
        self.first_run =  True

        for x in range(count_of_atoms):
            size = rand(5, 20)
            speed_x = rand(5, 20)
            speed_y = rand(5, 20)
            self.atoms.append(Atom(rand(0, self.width - size), rand(0, self.height - size), speed_x, speed_y, size))


    def tick(self):
        """This method is called by playground. Sends a tuple of atoms to rendering engine.
        
        :param size_x: world size x dimension
        :param size_y: world size y dimension
        :return: tuple of atom objects, each containing (x, y, radius) coordinates 
        """

        if not self.first_run:
            [a.move(self.width, self.height) for a in self.atoms ]
        else:
            self.first_run = False

        return list([a.to_tuple() for a in self.atoms])

    


if __name__ == '__main__':
    size_x, size_y = 400, 300

    world = ExampleWorld(size_x, size_y, 5)
    playground.run((size_x, size_y), world)
