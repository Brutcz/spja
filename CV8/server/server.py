from xmlrpc.server import SimpleXMLRPCServer

from decorator import append


class SPJAServer(object):

    def __init__(self):
        self.dct = {}
        self.filename = "/home/michal/Skola/VSB/SPJA/CV8/server/points.txt"
        self.load_data()

    def download_data(self):
        return self.dct

    def load_data(self):
        #TODO 1 (1b)
        #nacte data ze souboru points.txt
        #data ulozte do slovniku self.dct
        with open(self.filename, 'r') as pfile:
            for line in pfile:
                if line == '\n':
                    continue
                (login, points) = line.split(':')
                login = login.strip()
                points = points.strip().split(' ')
                self.dct[login] = list(map(lambda x: int(x), points))

    def save_data(self):
        #TODO 2 (1b)
        #ulozi data do souboru ve spravnem formatu (viz soubor points.txt)
        #data jsou ulozena ve slovniku self.dct
        with open(self.filename, 'w') as pfile:
            for key, val in self.dct.items():
                points = ' '.join(str(x) for x in val)
                line = '{} : {}\n'.format(key, points)
                pfile.write(line)

            

    def insert(self, login, points):
        #TODO 3 (1b)
        # vlozi vysledek studentovi (nezapomente osetrit, pokud student jeste neni v seznamu)
        # po pridani vysledku ulozte data (save_data)
        if login in self.dct:
            self.dct[login].append(points)
        else:
            self.dct[login] = [points]

        self.save_data()

        return 'OK'
    
    def get_best_6(self, login):
        #TODO 4 (1.5b)
        #vrati soucet 6-ti nejlepsich vysledku studenta
        if login in self.dct:
            return sorted(self.dct[login], reverse=True)[:6]
        else:
            return 0
            
def main():

    instance = SPJAServer()
    server_address = ('localhost', 10001)
    server = SimpleXMLRPCServer(server_address)
    
    server.register_function(instance.download_data, 'download_data')
    server.register_function(instance.insert, 'insert')
    server.register_function(instance.get_best_6, 'get_best_6')
    
    server.register_introspection_functions()
    
    print("Starting SPJA server, use <Ctrl-C> to stop")
    server.serve_forever()
    
if __name__ == "__main__":
    main()
