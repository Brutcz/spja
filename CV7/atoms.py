import playground
import random
import xml.etree.ElementTree as ET

class Atom(object):
    def __init__(self, pos_x, pos_y, speed_x, speed_y, size, color):
        self.pos_x = float(pos_x)
        self.pos_y = float(pos_y)
        self.size = float(size)
        self.speed_x = float(speed_x)
        self.speed_y = float(speed_y)
        self.color = color

    def to_tuple(self):
        return (self.pos_x, self.pos_y, self.size, self.color)

    def move(self, width, height):
        self.pos_x += self.speed_x
        self.pos_y += self.speed_y
        if self.pos_x + self.size > width:
            self.speed_x = -self.speed_x
            self.pos_x = width - self.size
        if self.pos_x < self.size:
            self.speed_x = -self.speed_x
            self.pos_x = self.size

        if self.pos_y + self.size > height:
            self.speed_y = -self.speed_y
            self.pos_y = height - self.size
        if self.pos_y < self.size:
            self.speed_y = -self.speed_y
            self.pos_y = self.size

class FallDownAtom(Atom):
    g = 3.0
    damping = 0.8
    
    def __init__(self, pos_x, pos_y, speed_x, speed_y, size, color):
        Atom.__init__(self, pos_x, pos_y, speed_x, speed_y, size, color)
    
    def move(self, width, height):
        self.speed_y += FallDownAtom.g
        
        self.pos_x += self.speed_x
        self.pos_y += self.speed_y

        if self.pos_x + self.size > width:
            self.speed_x = -self.speed_x
            self.pos_x = width - self.size
        if self.pos_x < self.size:
            self.speed_x = -self.speed_x
            self.pos_x = self.size

        if self.pos_y + self.size > height:
            self.speed_x = self.speed_x*FallDownAtom.damping
            self.speed_y = self.speed_y*FallDownAtom.damping
            self.speed_y = -self.speed_y
            self.pos_y = height - self.size
        if self.pos_y < self.size:
            self.speed_y = -self.speed_y
            self.pos_y = self.size
        

class ExampleWorld(object):

    def __init__(self, count, width, height):
        self.size_x = width
        self.size_y = height
        self.fallDownAtoms = 0
        self.normalAtoms = 0
        self.atoms = []
        self.load_from_xml('/home/michal/Skola/VSB/SPJA/CV7/atoms.xml')

    def load_from_xml(self, filename):
        tree = ET.parse(filename)
        root = tree.getroot()

        world = root.find('world')
        self.size_x = int(world.attrib['sizeX'])
        self.size_y = int(world.attrib['sizeY'])

        for atom in root.findall('atom'):
            if atom.attrib['type'] == "FallDown":
                self.fallDownAtoms += 1
                args = [
                    atom.find('position').text.split(',')[0],
                    atom.find('position').text.split(',')[1],
                    atom.find('velocity').text.split(',')[0],
                    atom.find('velocity').text.split(',')[1],
                    atom.find('radius').text,
                    atom.find('color').text,
                ]
                self.atoms.append(FallDownAtom(*args))
            if atom.attrib['type'] == "Normal":
                self.normalAtoms += 1
                args = [
                    atom.find('position').text.split(',')[0],
                    atom.find('position').text.split(',')[1],
                    atom.find('velocity').text.split(',')[0],
                    atom.find('velocity').text.split(',')[1],
                    atom.find('radius').text,
                    atom.find('color').text,
                ]
                self.atoms.append(Atom(*args))

    def tick(self, size_x, size_y):
        ret = []
        for atom in self.atoms:
            atom.move(size_x, size_y)
            ret.append(atom.to_tuple())
        return tuple(ret)
    
    def get_size(self):
        return (self.size_x, self.size_y)

    def __str__(self):
        return 'World size = {}x{}\n\tNumber of normal atoms = {}\n\tNumber of falldown atoms = {}'.format(self.size_x, self.size_y, self.normalAtoms, self.fallDownAtoms)


if __name__ == '__main__':
    size_x, size_y = 400, 300
    world = ExampleWorld(10, size_x, size_y)
    print(world)
    playground.run(world.get_size(), world)
