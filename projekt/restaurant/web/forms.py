from .models import Food
from django import forms
from django.conf import settings
from django.forms.widgets import DateInput


class LoginForm(forms.Form):
    username = forms.CharField(max_length=64)
    password = forms.CharField(widget=forms.PasswordInput())


class OrderForm(forms.Form):
    to_date = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS)
    food = forms.ModelChoiceField(queryset=Food.objects.all())


class FeedbackForm(forms.Form):
    text = forms.CharField(widget=forms.Textarea())
    food = forms.CharField(widget=forms.HiddenInput())