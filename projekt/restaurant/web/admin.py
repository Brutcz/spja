from django.contrib import admin

from .models import Food, Order, WebContent

admin.site.register(Food)
admin.site.register(Order)
admin.site.register(WebContent)