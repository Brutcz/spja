from django.db import models
from django.contrib.auth.models import User

class Food(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64)
    image = models.ImageField(upload_to='food_pic/')
    price = models.DecimalField(decimal_places=2, max_digits=5)
    desc = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

class WebContent(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64)
    content = models.TextField()

    def __str__(self):
        return self.name

class Order(models.Model):
    id = models.AutoField(primary_key=True)
    to_date = models.DateField(auto_now=True)
    food = models.ForeignKey(Food, on_delete=models.DO_NOTHING)
    customer = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateField(auto_now=True)

    def __str__(self):
        return '#{} - {} {}'.format(self.id, self.to_date, self.food.name)

class Feedback(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    food = models.ForeignKey(Food, on_delete=models.CASCADE)
    text = models.TextField()
    date = models.DateField(auto_now=True)

    def __str__(self):
        return '{} {} - {}'.format(self.user.first_name, self.user.last_name, self.food.name)