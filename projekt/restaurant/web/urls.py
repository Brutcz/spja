from django.urls import path
from .views import Index, FoodList, Food, OrderList, Order, OrderFood, Login, Logout

urlpatterns = [
    path('', Index.as_view(), name="index"),
    path('foodList', FoodList.as_view(), name="food-list"),
    path('food/<str:name>', Food.as_view(), name="food"),
    path('orderList', OrderList.as_view(), name="order-list"),
    path('order/<int:id>', Order.as_view(), name="order"),
    path('orderFood', OrderFood.as_view(), name="order-food"),
    path('login', Login.as_view(), name="login"),
    path('logout', Logout.as_view(), name="logout"),
]