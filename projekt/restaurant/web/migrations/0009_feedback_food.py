# Generated by Django 2.1.3 on 2018-11-21 14:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0008_feedback'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedback',
            name='food',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='web.Food'),
            preserve_default=False,
        ),
    ]
