# Generated by Django 2.1.3 on 2018-11-20 20:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0004_order_cutomer'),
    ]

    operations = [
        migrations.AlterField(
            model_name='food',
            name='image',
            field=models.ImageField(upload_to='food_pic/'),
        ),
    ]
