from .models import Food as FoodModel, Order as OrderModel, Feedback as FeedbackModel
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.decorators import method_decorator
from django.views import View

from .forms import LoginForm, OrderForm, FeedbackForm

class Index(View):
    def get(self, request):
        return render(request, 'index.html')

class Food(View):
    form_class = FeedbackForm
    template_name = 'food.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        food = get_object_or_404(FoodModel, name=kwargs['name'])
        reviews = FeedbackModel.objects.filter(food=food)
        context = { 'food': food, 'form': self.form_class(initial={'food': food.name}), 'reviews': reviews }
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            food = FoodModel.objects.get(name=form.data['food'])
            feedback = FeedbackModel(user=request.user, food=food, text=form.data['text'])
            feedback.save()
            return redirect('food', food.name)

        return render(request, self.template_name, {'form': form})
    
class FoodList(View):
    def get(self, request):
        context = {
            'foods' : FoodModel.objects.all()
        }
        return render(request, 'foodList.html', context)

class Login(View):
    form_class = LoginForm
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('index')

        return render(request, self.template_name, {'form': form})

class Logout(View):
    def get(self, request):
        logout(request)
        return redirect('index')

@method_decorator(login_required, name='dispatch')
class OrderFood(View):
    form_class = OrderForm
    template_name = 'orderFood.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            order = OrderModel(to_date=form.data['to_date'], food=FoodModel.objects.get(pk=form.data['food']), customer=request.user)
            order.save()
            return redirect('order-list')

        return render(request, self.template_name, {'form': form})

@method_decorator(login_required, name='dispatch')
class OrderList(View):
    def get(self, request):
        context = { 'orders': OrderModel.objects.filter(customer=request.user) }
        return render(request, 'orderList.html', context)

@method_decorator(login_required, name='dispatch')
class Order(View):
    def get(self, request, id):
        obj = get_object_or_404(OrderModel, id=id)
        context = { 'order': obj }
        return render(request, 'order.html', context) 