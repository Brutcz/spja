import playground
import random

class Atom(object):
    def __init__(self, pos_x, pos_y, speed_x, speed_y, size, color):
        self.pos_x = float(pos_x)
        self.pos_y = float(pos_y)
        self.size = float(size)
        self.speed_x = float(speed_x)
        self.speed_y = float(speed_y)
        self.color = color

    def to_tuple(self):
        return (self.pos_x, self.pos_y, self.size, self.color)

    def move(self, width, height):
        self.pos_x += self.speed_x
        self.pos_y += self.speed_y

        if self.pos_x + self.size > width:
            self.speed_x = -self.speed_x
            self.pos_x = width - self.size

        if self.pos_x < self.size:
            self.speed_x = -self.speed_x
            self.pos_x = self.size

        if self.pos_y + self.size > height:
            self.speed_y = -self.speed_y
            self.pos_y = height - self.size

        if self.pos_y < self.size:
            self.speed_y = -self.speed_y
            self.pos_y = self.size
        

class FallDownAtom(Atom):
    g = 3.0
    damping = 0.8

    def move(self, width, height):
        self.speed_y += FallDownAtom.g

        self.pos_x += self.speed_x
        self.pos_y += self.speed_y

        if self.pos_x + self.size > width:
            self.speed_x = -self.speed_x
            self.pos_x = width - self.size

        if self.pos_x < self.size:
            self.speed_x = -self.speed_x
            self.pos_x = self.size

        if self.pos_y + self.size > height:
            self.speed_x = self.speed_x*FallDownAtom.damping
            self.speed_y = self.speed_y*FallDownAtom.damping
            self.speed_y = -self.speed_y
            self.pos_y = height - self.size

        if self.pos_y < self.size:
            self.speed_y = -self.speed_y
            self.pos_y = self.size


class ExampleWorld(object):

    def __init__(self, count, width, height):
        self.size_x = width
        self.size_y = height
        self.atoms = []
        for x in range(count):
            self.atoms.append(self.random_atom())
    
    def random_atom(self):
        atom_type = random.randint(0, 1)

        pos_x = random.randint(0, 400)
        pos_y = random.randint(0, 300)
        speed_x = random.randint(2, 15)
        speed_y = random.randint(2, 15)
        size = random.randint(5, 35)
        color = random.choice(list(playground.Colors)).value

        if atom_type == 0:
            return Atom(pos_x, pos_y, speed_x, speed_y, size, color)
        
        if atom_type == 1:
            return FallDownAtom(pos_x, pos_y, speed_x, speed_y, size, color)
    
    def tick(self, size_x, size_y):
        ret = []
        for atom in self.atoms:
            atom.move(size_x, size_y)
            ret.append(atom.to_tuple())
        return tuple(ret)


if __name__ == '__main__':
    size_x, size_y = 400, 300
    world = ExampleWorld(10, size_x, size_y)
    playground.run((size_x, size_y), world)
