# import xmlrpc.client
from xmlrpc.client import ServerProxy
import random
import visualizer
import xml.etree.ElementTree as ET

# BEFORE YOU START
# 1) SET VIRTUAL ENVIRONMENT
# python3 -m venv spja
# . spja/bin/activate
# pip3 install pygame

class Agent:
    def __init__(self, filename):
        # TODO 1 1b - nacist data z configuracniho xml souboru (nazev v parametru
        # filename)
        tree = ET.parse(filename)
        root = tree.getroot()
        # TODO 2 0.5b - vytvorit instancni promenne (login, data, gameserver)
        # login - nastavit dle hodnoty prectene z XML
        # data - prazdny list, kde budou ukladana data ze serveru
        # gameserver - pripojit se k XML-RPC serveru (url v XML souboru)
        self.login = root.find('login').text
        url = root.find('url').text
        self.gameserver = ServerProxy(url)
        self.visualizer = visualizer.Visualizer()
        # TODO 3 0.5p - na serveru zavolat metodu add_player s parametrem login (v instancni promenne login)
        self.gameserver.add_player(self.login)

    def action(self):
        # TODO 4 0.5p - zavolat z xml-rpc serveru metodu make_action
        # metoda ma 3 parametry (login, nazev_akce, parametry)
        # vsechny tri jsou retezce. Zavolejte "look" na rozhlizeni
        # kolem sebe bez parametru (prazdny retezec). Metoda make-action vraci pole
        # retezcu, kde kazdy radek reprezentuje jeden radek z okoli.
        # Tato data ulozte do instancni promenne data a vykreslete pomoci metody visualize z tridy Visualizer.
        # Kazdy retezec ma sirku 11 znaku a samotnych radku je 22. Prvnich
        # jedenact reprezentuje okoli agenta a dalsich jedenact, objekty v
        # jeho okoli (zatim pouze "p" - ostatni agenti). Hrac je v tomto
        # okoli na pozici [5][5] (paty radek, paty znak). Objekty na stejne
        # pozici neleznete na souradnicich [5+11][5].
        # "~" voda
        # " " trava
        # "*" cesta
        # "t" strom
        # "o" skala (zed)
        # "f" dlazdena podlaha
        # "p" hrac
        self.data = self.gameserver.make_action(self.login, 'look', '')
        self.visualizer.visualize(self.data)

class AgentRandom(Agent):
    # TODO 5 1b - tento agent bude dedit z agenta predchoziho a
    # upravi funkci action tak, ze akci bude "move" a v parametru
    # preda jeden ze smeru "north", "west", "south", "east". Tyto
    # smery bude vybirat nahodne (najdete vhodnou metodu z balicku
    # random)
    def action(self):
        directions = ["north", "west", "south", "east"]

        self.data = self.gameserver.make_action(self.login, 'move', random.choice(directions))
        self.visualizer.visualize(self.data)



class AgentLeftRight(Agent):
    # TODO 6 1.5b - tento agent bude chodit stale doleva, dokud nenarazi na prekazku (co je a neni prekazka viz komentar v metode action, 
    # zajimaji vas pozice [5][4] a [5][6]).
    # V takovem pripade obrati svuj smer a pohybuje se stale doprava, nez take narazi na prekazku.
    def __init__(self, filename):
        self.direction = 'west'
        return super().__init__(filename)
    def action(self):
        self.data = self.gameserver.make_action(self.login, 'move', self.direction)

        if self.direction == 'west':
            if self.data[5][4] in ['~', 't', 'o', 'p']:
                self.direction = 'east'
        if self.direction == 'east':
            if self.data[5][6] in ['~', 't', 'o', 'p']:
                self.direction = 'west'

        self.visualizer.visualize(self.data)

def main():
    agent = None
    try:
        #agent = Agent("/home/michal/Skola/VSB/SPJA/CV9/config.xml")
        #agent = AgentRandom("/home/michal/Skola/VSB/SPJA/CV9/config.xml")
        agent = AgentLeftRight("/home/michal/Skola/VSB/SPJA/CV9/config.xml")
        while agent.visualizer.running:
            agent.action()
        else:
            agent.gameserver.make_action(agent.login, "exit", "")
    except KeyboardInterrupt:
        agent.gameserver.make_action(agent.login, "exit", "")


if __name__ == "__main__":
    main()
