#DEFINE THE FUNCTIONS ACCORDING TO THE ASSIGMENT

def filter_numbers(arr):
    return sorted([x for x in arr if isinstance(x, (int, float)) and type(x) is not bool])

def average_length(arr):
    strlens = sum(map(lambda x: len(x), arr))
    return strlens/len(arr)

def overlapping(arr, arr2):
    for x in arr:
        if x in arr2:
            return True

    return False

def number_of_letters(text):
    d = {}

    for key in list(set(text)):
        if key not in d:
            d[key] = 0

    for x in text:
        d[x] += 1

    return d 

def f(x):
    return x**2 - 2*x

def minmax(fn, d, i):
    if d > i:
        return False
    nums = list(map(f, range(d, i)))

    return(min(nums), max(nums))

#UNCOMMENT THE FOLLOWING FUNCTION CALLS
print(filter_numbers([1.2, "sdas", 4, [12], 3.4, "12", -3, True, 5, 8.1]))
print(average_length(["plzen", "liberec", "ostrava", "praha", "brno"]))
print(overlapping([1,2,3], [4,5,6]))
print(overlapping([1,2,3], [3,4,5]))
print(number_of_letters("ababdacabbdabc"))
print(minmax(f, -5, 5))
