import os.path


def add_line_numbers(filename):
    if os.path.isfile('n_{}'.format(filename)):
        raise Exception('File already exists')

    outFile = open('n_{}'.format(filename), 'w')

    with open(filename, 'r') as inFile:
        for index, line in enumerate(inFile):
            outFile.write('{} {}'.format(index+1, line))

    outFile.close()

def my_filtered_map(arr, fn, **kwargs):
    narr = [fn(x) for x in arr if type(x) is int or type(x) is float]
    if 'max' in kwargs:
        narr = [x for x in narr if x <= kwargs['max']]
    
    if 'min' in kwargs:
        narr = [x for x in narr if x >= kwargs['min']]

    return narr

def bank_account(*kargs):
    d = {}
    for filename in kargs:
        if not os.path.isfile(filename):
            print('File {} not exists'.format(filename))
            continue

        with open(filename, 'r') as file:
            for line in file:
                if '\n' in line:
                    line = line.replace('\n', '')
                
                (acc, action, suma) = line.split(' ')
                suma = float(suma)

                if acc not in d:
                    d[acc] = 0.0

                if action == 'D':
                    d[acc] += suma
                if action == 'W':
                    d[acc] -= suma

        
    return d

try:
    add_line_numbers("text.txt")
except Exception as e:
    print(e)

print(my_filtered_map([1,2,3,"x",5,8,13], lambda x: x*2, min=5, max=20))
print(my_filtered_map([True, -2.2, -1, 0, 1, 2], lambda x: x*2, max=0))

print(bank_account("bank_01.txt", "bank_02.txt"))
