import requests
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.generic import View

from .data import champions, champions_map
from .models import *


def fetch_user_data(name):
    res = requests.get('{}lol/summoner/v4/summoners/by-name/{}?api_key={}'.format(settings.LOL_API_BASE_URL, name, settings.LOL_API_KEY))
    if res.status_code == 404:
        raise Http404

    try:
        data = res.json()
        player, created = Summoner.objects.update_or_create(id=data['id'], defaults=data,)
        player.save()
    except Exception as e:
        print(e)


def fetch_league_data(name):
    player = Summoner.objects.get(name=name)
    res = requests.get('{}lol/league/v4/positions/by-summoner/{}?api_key={}'.format(settings.LOL_API_BASE_URL, player.id, settings.LOL_API_KEY))
    if res.status_code == 404:
        raise Http404

    try:
        data = res.json()
        for l in list(data):
            l['summonerId'] = player
            obj, created = League.objects.update_or_create(leagueId=l['leagueId'], defaults=l,)
            obj.save()
    except Exception as e:
        print(e)

def fetch_user_champions(name):
    player = Summoner.objects.get(name=name)
    res = requests.get('{}lol/champion-mastery/v4/champion-masteries/by-summoner/{}?api_key={}'.format(settings.LOL_API_BASE_URL, player.id, settings.LOL_API_KEY))
    if res.status_code == 404:
        raise Http404

    try:
        data = res.json()
        for ch in list(data):
            ch['summonerId'] = player
            obj, created = Champion.objects.update_or_create(championId=ch['championId'], summonerId=player, defaults=ch,)
            obj.save()
    except Exception as e:
        print(e)

class IndexView(View):
    def get(self, request, **kwargs):
        return render(request, 'index.html')


class SearchForPlayer(View):
    def get(self, request, **kwargs):
        name = request.GET.get('name')
        try:
            Summoner.objects.get(name=name)
        except ObjectDoesNotExist:
            fetch_user_data(name)
            fetch_league_data(name)
            fetch_user_champions(name)
        finally:
            return HttpResponseRedirect(reverse('web:player', args=[name]))


class PlayerView(View):
    def get(self, request, name, **kwargs):
        try:
            player = get_object_or_404(Summoner, name=name)
            fetch_league_data(name)
            fetch_user_champions(name)
            return render(request, 'player.html', {'player': player, 'leagues': player.league_set.all(), 'champions': player.champion_set.all(), })
        except Http404:
            return render(request, 'player.html', { 'player': None })

class ChampRotation(View):
    def get(self, request, **kwargs):
        r = requests.get('{}/lol/platform/v3/champion-rotations?api_key={}'.format(settings.LOL_API_BASE_URL, settings.LOL_API_KEY))
        data = r.json()
        for key in ['freeChampionIds', 'freeChampionIdsForNewPlayers']:
            tmp = []
            for ch in data[key]:
                if str(ch) in champions_map:
                    tmp.append(champions_map[str(ch)])
            data[key] = tmp
        
        return render(request, 'rotation.html', { 'data': data })


class ChampDetail(View):
    def get(self, request, name, **kwargs):
        champ = champions[name] if name in champions else None
        players = None
        try:
            ch = list(Champion.objects.filter(championId=int(champ['key'])).order_by('-championLevel'))
            players = [ (x.summonerId, x.championLevel) for x in ch ]
        except:
            pass

        return render(request, 'champ.html', { 'champ': champ, 'players': players })