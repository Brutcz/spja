import json
import os

from django.conf import settings

champions = []
champions_map = {}

with open(os.path.join(settings.BASE_DIR, "web" ,"champions.json")) as in_f:
  champions = json.load(in_f)['data']


  for champ in champions:
      champions_map[champions[champ]['key']] = champions[champ]