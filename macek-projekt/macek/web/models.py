from django.conf import settings
from django.db import models
import requests

from .data import champions, champions_map

class Summoner(models.Model):
    id = models.CharField(max_length=255, primary_key=True)
    accountId = models.CharField(max_length=255)
    puuid = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    profileIconId = models.IntegerField()
    revisionDate = models.BigIntegerField()
    summonerLevel = models.IntegerField()

    @property
    def get_profile_icon(self):
        return '{}{}.png'.format(settings.LOL_ICON_URL, self.profileIconId)

    def __str__(self):
        return self.name


class League(models.Model):
    leagueId = models.CharField(max_length=255)
    leagueName = models.CharField(max_length=255)
    queueType = models.CharField(max_length=255)
    position = models.CharField(max_length=255)
    tier = models.CharField(max_length=255)
    rank = models.CharField(max_length=255)
    leaguePoints = models.IntegerField()
    wins = models.IntegerField()
    losses = models.IntegerField()
    veteran = models.BooleanField()
    inactive = models.BooleanField()
    freshBlood = models.BooleanField()
    hotStreak = models.BooleanField()
    summonerId = models.ForeignKey(Summoner, on_delete=models.CASCADE)
    summonerName = models.CharField(max_length=255)

class Champion(models.Model):
    championId = models.IntegerField()
    championLevel = models.IntegerField()
    championPoints = models.IntegerField()
    lastPlayTime = models.IntegerField()
    championPointsSinceLastLevel = models.IntegerField()
    championPointsUntilNextLevel = models.IntegerField()
    chestGranted = models.BooleanField()
    tokensEarned = models.IntegerField()
    summonerId = models.ForeignKey(Summoner, on_delete=models.CASCADE)

    @property
    def champion_data(self):
        try:
            ch = champions_map[str(self.championId)]
            return ch
        except Exception as e:
            print("CHAMP_DATA",e)
            return None