from django.urls import path
from .views import *

app_name = 'web'

urlpatterns = [
    path('', IndexView.as_view(), name="index"),
    path('search/', SearchForPlayer.as_view(), name="search"),
    path('player/<slug:name>', PlayerView.as_view(), name="player"),
    path('champ/<slug:name>', ChampDetail.as_view(), name="champ"),
    path('champRotation/', ChampRotation.as_view(), name="rotation"),
]