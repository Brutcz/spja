# Generated by Django 2.1.3 on 2019-01-23 19:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Champion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('championId', models.IntegerField()),
                ('championLevel', models.IntegerField()),
                ('championPoints', models.IntegerField()),
                ('lastPlayTime', models.IntegerField()),
                ('championPointsSinceLastLevel', models.IntegerField()),
                ('championPointsUntilNextLevel', models.IntegerField()),
                ('chestGranted', models.BooleanField()),
                ('tokensEarned', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='League',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('leagueId', models.CharField(max_length=255)),
                ('leagueName', models.CharField(max_length=255)),
                ('queueType', models.CharField(max_length=255)),
                ('position', models.CharField(max_length=255)),
                ('tier', models.CharField(max_length=255)),
                ('rank', models.CharField(max_length=255)),
                ('leaguePoints', models.IntegerField()),
                ('wins', models.IntegerField()),
                ('losses', models.IntegerField()),
                ('veteran', models.BooleanField()),
                ('inactive', models.BooleanField()),
                ('freshBlood', models.BooleanField()),
                ('hotStreak', models.BooleanField()),
                ('summonerName', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Summoner',
            fields=[
                ('id', models.CharField(max_length=255, primary_key=True, serialize=False)),
                ('accountId', models.CharField(max_length=255)),
                ('puuid', models.CharField(max_length=255)),
                ('name', models.CharField(max_length=255)),
                ('profileIconId', models.IntegerField()),
                ('revisionDate', models.BigIntegerField()),
                ('summonerLevel', models.IntegerField()),
            ],
        ),
        migrations.AddField(
            model_name='league',
            name='summonerId',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='web.Summoner'),
        ),
        migrations.AddField(
            model_name='champion',
            name='summonerId',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='web.Summoner'),
        ),
    ]
